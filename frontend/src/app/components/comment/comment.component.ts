import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeleteComment } from 'src/app/models/comment/delete-comment';
import { CommentService } from 'src/app/services/comment.service';
import { Comment } from '../../models/comment/comment';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Output() commentRemoveAction: EventEmitter<Comment>;

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public display: boolean;
    private unsubscribe$ = new Subject<void>();

    public loadingComment = false;

    public constructor(private commentService: CommentService) {
        this.display = true;
        this.commentRemoveAction = new EventEmitter();
    }

    public deleteComment() {
        this.loadingComment = true;

        this.commentService.deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.commentRemoveAction.emit(this.comment);
                        this.removeComment();
                    }
                },
                (error) => { console.log(error); this.loadingComment = false; }
            );
    }

    private removeComment() {
        this.comment = null;
        this.display = false;
        this.loadingComment = false;
    }
}
