export interface DeleteComment {
    authorId: number;
    id: number;
    body: string;
}