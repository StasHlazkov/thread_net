import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { DeleteComment } from '../models/comment/delete-comment';
import { error } from '@angular/compiler/src/util';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public deleteComment(commentId: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${commentId}`);
    }
}
