﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_.NET.Common.DTO.Comment
{
    public class DeleteCommentDTO
    {
        public int AuthorId { get; set; }
        public int PostId { get; set; }
    }
}
